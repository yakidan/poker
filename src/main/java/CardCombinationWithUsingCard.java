import lombok.Getter;

public class CardCombinationWithUsingCard {
    @Getter
    private Card usingCard;
    @Getter
    private CardCombination cardCombination;

    public CardCombinationWithUsingCard(CardCombination cardCombination, Card usingCard) {
        this.usingCard = usingCard;
        this.cardCombination = cardCombination;
    }
}
