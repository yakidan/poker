import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class PokerHand implements Comparable<PokerHand> {
    @Getter
    private List<Card> cards;

    @Getter
    private CardCombinationWithUsingCard cardCombinationWithUsingCard;

    @Override
    public String toString() {
        return "PokerHand{" +
                "cards=" + cards +
                ", cardCombinationWithUsingCard=" + cardCombinationWithUsingCard +
                ", cardCombination=" + cardCombination +
                ", pokerService=" + pokerService +
                '}';
    }

    @Getter
    private CardCombination cardCombination;

    private PokerService pokerService = new PokerService();

    public PokerHand(String cardsInput) {
        cards = new ArrayList<>();
        init(cardsInput);
        cardCombinationWithUsingCard = pokerService.getCardCombination(cards);
        cardCombination = cardCombinationWithUsingCard.getCardCombination();
    }

    private void init(String input) {
        String[] cardsInput = input.split(" ");
        for (String cardInput :
                cardsInput) {
            CardNominal nominal = CardNominal.getCardNominal(cardInput.substring(0, 1));
            CardDenomination denomination = CardDenomination.valueOf(cardInput.substring(1, 2));
            Card card = new Card(nominal, denomination);
            cards.add(card);
        }
    }


    @Override
    public int compareTo(PokerHand other) {
        CardNominal thisHighUsingCardInCombination
                = this.cardCombinationWithUsingCard.getUsingCard().getNominal();
        CardNominal otherHighUsingCardInCombination
                = other.cardCombinationWithUsingCard.getUsingCard().getNominal();
        if (this.cardCombination == other.cardCombination) {
            return thisHighUsingCardInCombination.getValue()
                    - otherHighUsingCardInCombination.getValue();

        }
        return this.cardCombination.getWeight() - other.cardCombination.getWeight();
    }
}
