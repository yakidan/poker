import lombok.Getter;

import java.util.Arrays;

public enum CardDenomination {
    S("S"),
    H("H"),
    D("D"),
    C("C"),
    ;
    @Getter
    private final String denomination;

    CardDenomination(String d) {
        this.denomination = d;
    }

}
