import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
@AllArgsConstructor
public class Card implements Comparable<Card> {
    @Override
    public String toString() {
        return "Card{" +
                "nominal=" + nominal +
                ", denomination=" + denomination +
                '}';
    }

    @Getter
    private CardNominal nominal;
    @Getter
    private CardDenomination denomination;

    @Override
    public int compareTo(Card o) {
        return this.nominal.getValue() - o.nominal.getValue();
    }
}
