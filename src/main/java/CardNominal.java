import lombok.Getter;

import java.util.Arrays;
import java.util.InvalidPropertiesFormatException;

public enum CardNominal {
    N_2(2),
    N_3(3),
    N_4(4),
    N_5(5),
    N_6(6),
    N_7(7),
    N_8(8),
    N_9(9),
    T(10),
    J(11),
    Q(12),
    K(13),
    A(14);

    @Getter
    private final int value;

    CardNominal(int a) {
        this.value = a;
    }

    static public CardNominal getCardNominal(String nominal)  {
        try {
            Integer checkOnNumber = Integer.parseInt(nominal);
            return Arrays.stream(CardNominal.values())
                    .filter(o -> String.valueOf(o.getValue()).equals(nominal))
                    .findFirst()
                    .get();
        } catch (NumberFormatException ex) {
            return CardNominal.valueOf(nominal);
        }
    }
}
