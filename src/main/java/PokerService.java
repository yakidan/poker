import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PokerService {


    public Card getHighCard(List<Card> cards) {
        return cards.get(4);
    }

    public CardCombinationWithUsingCard getCardCombination(List<Card> cards) {
        Collections.sort(cards);

        if (checkRoyalFlush(cards) != null) {
            return checkRoyalFlush(cards);
        } else if (checkStraightFlush(cards) != null) {
            return checkStraightFlush(cards);
        } else if (checkFour(cards) != null) {
            return checkFour(cards);
        } else if (checkFlush(cards) != null) {
            return checkFlush(cards);
        } else if (checkStraight(cards) != null) {
            return checkStraight(cards);
        } else if (checkThree(cards) != null) {
            return checkThree(cards);
        } else if (checkTwoPair(cards) != null) {
            return checkTwoPair(cards);
        } else if (checkPair(cards) != null) {
            return checkPair(cards);
        }

        return new CardCombinationWithUsingCard(CardCombination.HIGH_CARD, getHighCard(cards));
    }


    CardCombinationWithUsingCard checkRoyalFlush(List<Card> cards) {
        if (isStraight(cards) && isFlush(cards) && getHighCard(cards).getNominal() == CardNominal.A)
            return new CardCombinationWithUsingCard(CardCombination.ROYAL_FLUSH, getHighCard(cards));
        return null;
    }

    CardCombinationWithUsingCard checkStraightFlush(List<Card> cards) {
        if (isStraight(cards) && isFlush(cards))
            return new CardCombinationWithUsingCard(CardCombination.STRAIGHT_FLUSH, getHighCard(cards));
        return null;
    }

    CardCombinationWithUsingCard checkFour(List<Card> cards) {
        if (isFour(cards)) {
            return new CardCombinationWithUsingCard(CardCombination.FOUR, getHighCardForNCombination(cards, 4));
        }
        return null;
    }


    CardCombinationWithUsingCard checkFlush(List<Card> cards) {
        if (isFlush(cards)) {
            return new CardCombinationWithUsingCard(CardCombination.FLUSH, getHighCard(cards));
        }
        return null;
    }

    CardCombinationWithUsingCard checkStraight(List<Card> cards) {
        if (isStraight(cards)) {
            return new CardCombinationWithUsingCard(CardCombination.STRAIGHT, getHighCard(cards));
        }
        return null;
    }

    CardCombinationWithUsingCard checkThree(List<Card> cards) {
        if (isThree(cards)) {
            return new CardCombinationWithUsingCard(CardCombination.THREE, getHighCardForNCombination(cards, 3));
        }
        return null;
    }


    CardCombinationWithUsingCard checkTwoPair(List<Card> cards) {
        Map<Integer, Integer> mapWithNonRepeat = getMapEntry(cards);
        int countPair = 0;
        CardNominal maxNominal = CardNominal.N_2;
        for (Map.Entry<Integer, Integer> entry :
                mapWithNonRepeat.entrySet()) {
            if (entry.getValue() == 2) {
                countPair++;
                if (entry.getKey() >= maxNominal.getValue()) {
                    maxNominal = CardNominal.getCardNominal(entry.getKey().toString());
                }
            }
        }
        if (countPair == 2) {
            CardNominal finalMaxNominal = maxNominal;
            return new CardCombinationWithUsingCard(CardCombination.TWO_PAIR, cards
                    .stream()
                    .filter(o -> o.getNominal() == finalMaxNominal)
                    .findFirst()
                    .get()
            );
        }
        return null;
    }

    CardCombinationWithUsingCard checkPair(List<Card> cards) {
        if (isTwo(cards)) {
            return new CardCombinationWithUsingCard(CardCombination.PAIR, getHighCardForNCombination(cards, 2));
        }
        return null;
    }

    private boolean isStraight(List<Card> cards) {
        Collections.sort(cards);
        for (int i = 1; i < cards.size(); i++) {
            Card cardPrev = cards.get(i - 1);
            Card cardCurrent = cards.get(i);
            if (cardCurrent.getNominal().getValue() - cardPrev.getNominal().getValue() != 1) {
                return false;
            }
        }
        return true;
    }

    private boolean isFlush(List<Card> cards) {
        var cardDenomination = cards.get(0).getDenomination();
        for (Card c :
                cards) {
            if (!(c.getDenomination() == cardDenomination))
                return false;
        }
        return true;
    }

    private boolean isFour(List<Card> cards) {
        if (maxCountOfSame(cards) == 4)
            return true;
        return false;
    }

    private boolean isThree(List<Card> cards) {
        if (maxCountOfSame(cards) == 3)
            return true;
        return false;
    }

    private boolean isTwo(List<Card> cards) {
        if (maxCountOfSame(cards) == 2)
            return true;
        return false;
    }

    private Card getHighCardForNCombination(List<Card> cards, int count) {
        Map<Integer, Integer> map = getMapEntry(cards);
        for (Map.Entry<Integer, Integer> entry :
                map.entrySet()) {
            if (entry.getValue() == count) {
                return cards
                        .stream()
                        .filter(o -> o.getNominal().getValue() == entry.getKey())
                        .findFirst()
                        .get();
            }
        }
        return null;
    }

    private int maxCountOfSame(List<Card> cards) {
        Map<Integer, Integer> mapNominal = new HashMap<>();
        for (Card c :
                cards) {
            var nominalValue = c.getNominal().getValue();
            if (!mapNominal.containsKey(nominalValue)) {
                mapNominal.put(nominalValue, 1);
            } else {
                mapNominal.put(nominalValue, mapNominal.get(nominalValue) + 1);
            }
        }
        Map.Entry<Integer, Integer> maxEntry = mapNominal.entrySet().stream().findFirst().get();
        for (Map.Entry<Integer, Integer> entry :
                mapNominal.entrySet()) {
            if (entry.getValue() > maxEntry.getValue()) {
                maxEntry = entry;
            }
        }
        return maxEntry.getValue();
    }


    private Map<Integer, Integer> getMapEntry(List<Card> cards) {
        Map<Integer, Integer> mapNominal = new HashMap<>();
        for (Card c :
                cards) {
            var nominalValue = c.getNominal().getValue();
            if (!mapNominal.containsKey(nominalValue)) {
                mapNominal.put(nominalValue, 1);
            } else {
                mapNominal.put(nominalValue, mapNominal.get(nominalValue) + 1);
            }
        }

        return mapNominal;
    }
}
