public enum CardCombination {
    HIGH_CARD(1),
    PAIR(2),
    TWO_PAIR(3),
    THREE(4),
    STRAIGHT(5),
    FLUSH(6),
    FOUR(7),
    STRAIGHT_FLUSH(8),
    ROYAL_FLUSH(9),
    ;

    public int getWeight() {
        return weight;
    }

    private int weight;

    CardCombination(int i) {
        this.weight = i;
    }
}
