import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;

public class TestSameCombination {

    @Test
    public void TwoStraightFlushCombinations() {
        String minInputCombination = "2S 3S 4S 5S 6S";
        String maxInputCombination = "TD JD 9D QD KD";
        PokerHand minPokerHand = new PokerHand(minInputCombination);
        PokerHand maxPokerHand = new PokerHand(maxInputCombination);
        ArrayList<PokerHand> pokerHandList = new ArrayList<>();
        pokerHandList.add(maxPokerHand);
        pokerHandList.add(minPokerHand);
        Collections.sort(pokerHandList);
        Assert.assertEquals(pokerHandList.get(0), minPokerHand);
        Assert.assertEquals(pokerHandList.get(1), maxPokerHand);
    }

    @Test
    public void TwoFourCombinations() {
        String minInputCombination = "2S 2H 2D 2C 6S";
        String maxInputCombination = "4S 4H 4D 4C 5D";
        PokerHand minPokerHand = new PokerHand(minInputCombination);
        PokerHand maxPokerHand = new PokerHand(maxInputCombination);
        ArrayList<PokerHand> pokerHandList = new ArrayList<>();
        pokerHandList.add(maxPokerHand);
        pokerHandList.add(minPokerHand);
        Collections.sort(pokerHandList);
        Assert.assertEquals(pokerHandList.get(0), minPokerHand);
        Assert.assertEquals(pokerHandList.get(1), maxPokerHand);
    }

    @Test
    public void TwoFlushCombinations() {
        String minInputCombination = "2S 4S 6S TS KS";
        String maxInputCombination = "4D 7D TD QD AD";
        PokerHand minPokerHand = new PokerHand(minInputCombination);
        PokerHand maxPokerHand = new PokerHand(maxInputCombination);
        ArrayList<PokerHand> pokerHandList = new ArrayList<>();
        pokerHandList.add(maxPokerHand);
        pokerHandList.add(minPokerHand);
        Collections.sort(pokerHandList);
        Assert.assertEquals(pokerHandList.get(0), minPokerHand);
        Assert.assertEquals(pokerHandList.get(1), maxPokerHand);
    }

    @Test
    public void TwoStraightCombinations() {
        String minInputCombination = "2S 3C 4D 5S 6S";
        String maxInputCombination = "3D 4S 5C 6D 7D";
        PokerHand minPokerHand = new PokerHand(minInputCombination);
        PokerHand maxPokerHand = new PokerHand(maxInputCombination);
        ArrayList<PokerHand> pokerHandList = new ArrayList<>();
        pokerHandList.add(maxPokerHand);
        pokerHandList.add(minPokerHand);
        Collections.sort(pokerHandList);
        Assert.assertEquals(pokerHandList.get(0), minPokerHand);
        Assert.assertEquals(pokerHandList.get(1), maxPokerHand);
    }

    @Test
    public void TwoThreeCombinations() {
        String minInputCombination = "3S 3C 3D 5S 6S";
        String maxInputCombination = "5D 5S 5C 6D 7D";
        PokerHand minPokerHand = new PokerHand(minInputCombination);
        PokerHand maxPokerHand = new PokerHand(maxInputCombination);
        ArrayList<PokerHand> pokerHandList = new ArrayList<>();
        pokerHandList.add(maxPokerHand);
        pokerHandList.add(minPokerHand);
        Collections.sort(pokerHandList);
        Assert.assertEquals(pokerHandList.get(0), minPokerHand);
        Assert.assertEquals(pokerHandList.get(1), maxPokerHand);
    }

    @Test
    public void TwoTwoPairCombinations() {
        String minInputCombination = "3S 3C 4D 4S 6S";
        String maxInputCombination = "2D 2S 6C 6D 7D";
        PokerHand minPokerHand = new PokerHand(minInputCombination);
        PokerHand maxPokerHand = new PokerHand(maxInputCombination);
        ArrayList<PokerHand> pokerHandList = new ArrayList<>();
        pokerHandList.add(maxPokerHand);
        pokerHandList.add(minPokerHand);
        Collections.sort(pokerHandList);
        Assert.assertEquals(pokerHandList.get(0), minPokerHand);
        Assert.assertEquals(pokerHandList.get(1), maxPokerHand);
    }

    @Test
    public void TwoPairCombinations() {
        String minInputCombination = "3S 3C TD 4S 6S";
        String maxInputCombination = "QD QS 2C 6D 7D";
        PokerHand minPokerHand = new PokerHand(minInputCombination);
        PokerHand maxPokerHand = new PokerHand(maxInputCombination);
        ArrayList<PokerHand> pokerHandList = new ArrayList<>();
        pokerHandList.add(maxPokerHand);
        pokerHandList.add(minPokerHand);
        Collections.sort(pokerHandList);
        Assert.assertEquals(pokerHandList.get(0), minPokerHand);
        Assert.assertEquals(pokerHandList.get(1), maxPokerHand);
    }

    @Test
    public void TwoHighCombinations() {
        String minInputCombination = "3S 2C TD 4S 6S";
        String maxInputCombination = "JD QS 2C 6D 7D";
        PokerHand minPokerHand = new PokerHand(minInputCombination);
        PokerHand maxPokerHand = new PokerHand(maxInputCombination);
        ArrayList<PokerHand> pokerHandList = new ArrayList<>();
        pokerHandList.add(maxPokerHand);
        pokerHandList.add(minPokerHand);
        Collections.sort(pokerHandList);
        Assert.assertEquals(pokerHandList.get(0), minPokerHand);
        Assert.assertEquals(pokerHandList.get(1), maxPokerHand);
    }


}
