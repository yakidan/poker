import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;


public class TestAllCombination {
    final String INPUT_ROYAL_FLUSH = "TS JS AS QS KS";
    final String INPUT_STRAIGHT_FLUSH = "TD JD 9D QD KD";
    final String INPUT_FOUR = "9S 9H 9D 9C TC";
    final String INPUT_FLUSH = "2C 4C 7C JC AC";
    final String INPUT_STRAIGHT = "8D 9C TH JC QD";
    final String INPUT_THREE = "2D 9C 9H 9D QD";
    final String INPUT_TWO_PAIR = "2D 2C 9H 9D QD";
    final String INPUT_PAIR = "3D 2C 9H 9D QD";
    final String INPUT_HIGH_CARD = "3D 2C TH 9D QD";

    @Test
    public void checkRoyalFlush() {
        PokerHand pokerHand = new PokerHand(INPUT_ROYAL_FLUSH);
        Assert.assertEquals(pokerHand.getCardCombination(), CardCombination.ROYAL_FLUSH);
    }

    @Test
    public void checkStraightFlush() {
        PokerHand pokerHand = new PokerHand(INPUT_STRAIGHT_FLUSH);
        Assert.assertEquals(pokerHand.getCardCombination(), CardCombination.STRAIGHT_FLUSH);
    }

    @Test
    public void checkFour() {
        PokerHand pokerHand = new PokerHand(INPUT_FOUR);
        Assert.assertEquals(pokerHand.getCardCombination(), CardCombination.FOUR);
    }

    @Test
    public void checkFlush() {
        PokerHand pokerHand = new PokerHand(INPUT_FLUSH);
        Assert.assertEquals(pokerHand.getCardCombination(), CardCombination.FLUSH);
    }

    @Test
    public void checkStraight() {
        PokerHand pokerHand = new PokerHand(INPUT_STRAIGHT);
        Assert.assertEquals(pokerHand.getCardCombination(), CardCombination.STRAIGHT);
    }

    @Test
    public void checkThree() {
        PokerHand pokerHand = new PokerHand(INPUT_THREE);
        Assert.assertEquals(pokerHand.getCardCombination(), CardCombination.THREE);
    }

    @Test
    public void checkTwoPair() {
        PokerHand pokerHand = new PokerHand(INPUT_TWO_PAIR);
        Assert.assertEquals(pokerHand.getCardCombination(), CardCombination.TWO_PAIR);
    }

    @Test
    public void checkPair() {
        PokerHand pokerHand = new PokerHand(INPUT_PAIR);
        Assert.assertEquals(pokerHand.getCardCombination(), CardCombination.PAIR);
    }

    @Test
    public void checkHighCard() {
        PokerHand pokerHand = new PokerHand(INPUT_HIGH_CARD);
        Assert.assertEquals(pokerHand.getCardCombination(), CardCombination.HIGH_CARD);
    }

    @Test
    public void TwoAllCombinations() {
        PokerHand pokerHandRoyalFlush = new PokerHand(INPUT_ROYAL_FLUSH);
        PokerHand pokerHandStraightFlush = new PokerHand(INPUT_STRAIGHT_FLUSH);
        PokerHand pokerHandFour = new PokerHand(INPUT_FOUR);
        PokerHand pokerHandFlush = new PokerHand(INPUT_FLUSH);
        PokerHand pokerHandStraight = new PokerHand(INPUT_STRAIGHT);
        PokerHand pokerHandThree = new PokerHand(INPUT_THREE);
        PokerHand pokerHandTwoPair = new PokerHand(INPUT_TWO_PAIR);
        PokerHand pokerHandPair = new PokerHand(INPUT_PAIR);
        PokerHand pokerHandHighCard = new PokerHand(INPUT_HIGH_CARD);
        ArrayList<PokerHand> pokerHandList = new ArrayList<>();
        pokerHandList.add(pokerHandRoyalFlush);
        pokerHandList.add(pokerHandStraightFlush);
        pokerHandList.add(pokerHandFour);
        pokerHandList.add(pokerHandFlush);
        pokerHandList.add(pokerHandStraight);
        pokerHandList.add(pokerHandThree);
        pokerHandList.add(pokerHandTwoPair);
        pokerHandList.add(pokerHandPair);
        pokerHandList.add(pokerHandHighCard);
        Collections.sort(pokerHandList);
        Assert.assertEquals(pokerHandList.get(0), pokerHandHighCard);
        Assert.assertEquals(pokerHandList.get(1), pokerHandPair);
        Assert.assertEquals(pokerHandList.get(2), pokerHandTwoPair);
        Assert.assertEquals(pokerHandList.get(3), pokerHandThree);
        Assert.assertEquals(pokerHandList.get(4), pokerHandStraight);
        Assert.assertEquals(pokerHandList.get(5), pokerHandFlush);
        Assert.assertEquals(pokerHandList.get(6), pokerHandFour);
        Assert.assertEquals(pokerHandList.get(7), pokerHandStraightFlush);
        Assert.assertEquals(pokerHandList.get(8), pokerHandRoyalFlush);
    }
}
